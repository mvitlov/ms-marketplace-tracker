import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderByComponent } from './order-by';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [OrderByComponent],
  imports: [IonicPageModule.forChild(OrderByComponent), TranslateModule.forChild()],
  exports: [OrderByComponent]
})
export class OrderByModule {}
