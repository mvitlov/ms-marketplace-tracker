import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'order-by',
  templateUrl: 'order-by.html'
})
export class OrderByComponent {
  public orderBy: string;
  constructor(private navParams: NavParams, private view: ViewController) {
    this.orderBy = this.navParams.get('orderBy');
  }

  public close(tag: string): void {
    this.view.dismiss(tag);
  }
}
