import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { ExploreCodePage } from './explore-code';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [ExploreCodePage],
  imports: [IonicPageModule.forChild(ExploreCodePage), TranslateModule.forChild(), ComponentsModule]
})
export class ExploreCodePageModule {}
