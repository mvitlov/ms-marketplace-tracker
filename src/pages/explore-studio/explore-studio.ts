import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, PopoverController, Content } from 'ionic-angular';
import { MarketplaceProvider, Settings } from '../../providers';
import { STUDIO_TOOLBAR } from '../../app/app.config';

@IonicPage()
@Component({
  selector: 'page-explore-studio',
  templateUrl: 'explore-studio.html'
})
export class ExploreStudioPage {
  @ViewChild(Content) content: Content;
  public viewData = {};
  public toolbarItems = STUDIO_TOOLBAR;
  public loading = true;
  public trending = 'TrendingDaily';
  public currentView = 'Explore';
  public orderBy = 4; // 4:"downloads" - popover menu parameter
  private prefetch = false;
  // data: any;

  constructor(
    public navCtrl: NavController,
    private popover: PopoverController,
    private mp: MarketplaceProvider,
    private settings: Settings
  ) {
    this.mp.getVisualStudioExtensions().subscribe(
      result => {
        result['epc'].forEach(element => {
          this.viewData[element.cn] = element.e;
        });
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    );
    this.settings.getValue('prefetchExtensions').then(prefetch => {
      if (prefetch) {
        this.prefetchExtensions();
        this.prefetch = prefetch;
      }
    });
  }

  public onTrendingChange(event: string): void {
    this.trending = event;
  }

  public switchView(button): void {
    this.content.scrollToTop();
    this.currentView = button.name;
    if (!this.prefetch && button.name != 'Explore') {
      this.loadExtensionsCategory(button.name);
    }
  }

  public checkActiveView(button): boolean {
    return button.name === this.currentView;
  }

  private prefetchExtensions(): void {
    STUDIO_TOOLBAR.slice(1).forEach(el => {
      this.mp.getVisualStudioExtensions(el.name).subscribe(res => {
        this.viewData[el.name] = res;
      });
    });
  }

  private loadExtensionsCategory(name: string) {
    this.loading = true;
    this.mp.getVisualStudioExtensions(name, 1, this.orderBy).subscribe(
      res => {
        this.viewData[name] = res;
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  /* PRESENT 'ORDER BY' POPOVER MENU */
  public presentMenu(event: any): void {
    const menu = this.popover.create('OrderByComponent', { orderBy: this.orderBy });

    menu.present({ ev: event });

    menu.onWillDismiss((order: number) => {
      if (order && order !== this.orderBy) {
        this.orderBy = order;
        this.loadExtensionsCategory(this.currentView);
      }
    });
  }
}
