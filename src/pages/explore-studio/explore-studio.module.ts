import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ExploreStudioPage } from './explore-studio';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [ExploreStudioPage],
  imports: [IonicPageModule.forChild(ExploreStudioPage), TranslateModule.forChild(), ComponentsModule]
})
export class ExploreStudioPageModule {}
