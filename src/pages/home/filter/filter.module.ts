import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { FilterMenu } from './filter';

@NgModule({
  declarations: [FilterMenu],
  imports: [IonicPageModule.forChild(FilterMenu), TranslateModule.forChild()]
})
export class FilterMenuModule {}
