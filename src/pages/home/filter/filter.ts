import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HomePage } from '../home';

@IonicPage()
@Component({
  selector: 'menu-filter',
  templateUrl: 'filter.html'
})
export class FilterMenu {
  private homePage: HomePage;
  public data = { filter: { vs: true, code: true, azure: true }, sort: 4 };

  constructor(private navParams: NavParams, public viewCtrl: ViewController) {
    this.data = navParams.get('data');
    this.homePage = navParams.get('homePage');
  }

  watchExt() {
    this.viewCtrl.dismiss('watch');
  }
  unWatchExt() {
    this.viewCtrl.dismiss('unwatch');
  }

  public onCheckChange(target: string, event: boolean): void {
    this.homePage.triggerFilter(this.data.filter);
  }

  close(sort) {
    this.viewCtrl.dismiss(sort);
  }
}
