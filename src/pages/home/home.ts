import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Refresher, Events } from 'ionic-angular';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';
import { MarketplaceProvider, FavoritesProvider, ToastProvider, Settings } from '../../providers';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { Extension } from '../../models/extension.model';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('listAnimation', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class HomePage {
  public loading: boolean = true;
  public refreshing: boolean = false;
  public favorites: Extension[];
  descending: boolean = false;
  column: string = 'extensionName';
  public sortFilter = { filter: { vs: true, code: true, azure: true }, sort: 4 };
  /* dummy pipe trigger | because pipe triggers only on different object instances */
  public sortFilterChange = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private mp: MarketplaceProvider,
    private tp: ToastProvider,
    private favs: FavoritesProvider,
    private events: Events,
    private popoverCtrl: PopoverController,
    private settings: Settings
  ) {}

  ionViewWillEnter(): void {
    this.getFavoriteExtensions();
  }

  public doRefresh(ref: Refresher): void {
    this.refreshing = true;
    this.getFavoriteExtensions(ref);
  }

  private getFavoriteExtensions(refresher?: Refresher): void {
    refresher ? (this.loading = false) : (this.loading = true);
    this.favs.getStoredExtensions().then(res => {
      if (res && res.length) {
        this.getExtensionsById(res, refresher);
      } else {
        refresher ? refresher.complete() : false;
        this.refreshing = false;
        this.favorites = [];
        this.loading = false;
      }
    });
  }

  public extensionDetails(ext: Extension): void {
    this.navCtrl.push('ExtensionDetailsPage', {
      extension: ext
    });
  }

  public goToSearch(): void {
    this.navCtrl.push(
      'SearchPage',
      {},
      {
        animate: true,
        direction: 'forward'
      }
    );
  }

  public presentMenu(event): void {
    let menu = this.popoverCtrl.create('FilterMenu', { data: this.sortFilter, homePage: this });
    menu.present({
      ev: event
    });
    menu.onWillDismiss((sort: number) => {
      if (sort) {
        this.triggerSort(sort);
      }
    });
  }

  triggerFilter(filter) {
    this.sortFilter.filter = filter;
    this.sortFilterChange++;
  }
  triggerSort(sort) {
    this.sortFilter.sort = sort;
    this.sortFilterChange++;
  }

  private getExtensionsById(res: Array<string>, refresher?) {
    let tempArray: Extension[] = (this.favorites = []);
    res.forEach((id: string) => {
      this.mp.getExtensionById(id).subscribe(
        extension => {
          tempArray.push(extension);
        },
        err => {
          console.log(err);
        },
        () => {
          // show results only after all extensions are loaded
          if (res.length === tempArray.length) {
            refresher ? refresher.complete() : false;
            this.refreshing = false;
            this.loading = false;
            this.favorites = tempArray;
            this.sortFilterChange++;
          }
        }
      );
    });
  }
}
