import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';

import { MarketplaceProvider } from '../../providers';
import { Azure, VisualStudio, Code } from '../../models/targets.model';
import { Extension } from '../../models/extension.model';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
  animations: [
    trigger('listAnimation', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '50ms',
              animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' }))
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class SearchPage {
  searching: boolean = false;
  searchResults: Extension[];

  constructor(private mp: MarketplaceProvider, public navCtrl: NavController) {
    // for dev only, remove for prod
    this.onSearchInput({ target: { value: 'firebird' } });
  }

  public openExtension(ext): void {
    this.navCtrl.push('ExtensionDetailsPage', {
      extension: ext
    });
  }

  public onSearchInput(event): void {
    this.searchResults = [];
    this.searching = true;
    this.mp.searchExtensions(event.target.value).subscribe(
      data => {
        if (data) {
          data['results'][0].extensions.forEach(extension => {
            this.searchResults.push(new Extension(extension));
          });
        } else {
          this.searchResults = [];
        }
        this.searching = false;
      },
      err => {
        console.log(err);
        this.searching = false;
      },
      () => {
        this.searching = false;
      }
    );
  }
}
