import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Items } from '../../providers';
import { temp1 } from './temp1';
@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  item: any;
  extension: any;

  //
  section: string = 'two';
  somethings: any = new Array(20);

  constructor(public navCtrl: NavController, navParams: NavParams, items: Items) {
    // this.item = navParams.get('item') || items.defaultItem;
    // this.extension = navParams.get('extension');
    this.extension = JSON.parse(temp1);
    console.log(this.extension);
  }
}
