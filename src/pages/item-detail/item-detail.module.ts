import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ItemDetailPage } from './item-detail';

import { ShrinkingSegmentHeader } from './shrinking/shrinking';

@NgModule({
  declarations: [ItemDetailPage, ShrinkingSegmentHeader],
  imports: [IonicPageModule.forChild(ItemDetailPage), TranslateModule.forChild()],
  exports: [ItemDetailPage]
})
export class ItemDetailPageModule {}
