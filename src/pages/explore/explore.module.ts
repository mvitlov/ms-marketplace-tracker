import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ExplorePage } from './explore';

@NgModule({
  declarations: [ExplorePage],
  imports: [IonicPageModule.forChild(ExplorePage), TranslateModule.forChild()]
})
export class ExplorePageModule {}
