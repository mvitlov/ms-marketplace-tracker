import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';

import { extensionUrl } from '../../../app/app.config';
import { ToastProvider } from '../../../providers';
import { SocialProvider } from '../../../providers/social/social';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {
  data: any;
  constructor(
    private navParams: NavParams,
    public viewCtrl: ViewController,
    private clipboard: Clipboard,
    private tp: ToastProvider,
    private social: SocialProvider
  ) {
    this.data = navParams.data;
  }

  watchExt() {
    this.viewCtrl.dismiss('watch');
  }
  unWatchExt() {
    this.viewCtrl.dismiss('unwatch');
  }

  public shareExt() {
    let target: string;
    let url = extensionUrl + this.data.url;
    switch (this.data.target) {
      case 'vs': {
        target = 'Visual Studio';
        break;
      }
      case 'code': {
        target = 'VS Code';
        break;
      }
      case 'azure': {
        target = 'Azure DevOps';
        break;
      }
    }
    const extOptions = {
      message: `Hey, check out this cool extension for ${target}\nLink:\n`,
      subject: this.data.name,
      file: this.data.file,
      url: url,
      chooserTitle: 'Choose an app'
    };

    this.social.shareApp(extOptions);
    this.close();
  }

  public copyUrl(): void {
    let url = extensionUrl + '.' + this.data.url;
    this.clipboard.copy(url).then(() => {
      this.tp.presentToast('URL copied to clipboard!', 3000, 'bottom');
      this.close();
    });
  }

  public openInBrowser() {
    let url = extensionUrl + this.data.url;
    this.social.openWebPage(url);
    this.close();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
