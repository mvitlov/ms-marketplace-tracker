import { Component, Input } from '@angular/core';

@Component({
  selector: 'star-rating',
  templateUrl: 'rating.component.html'
})
export class StarRating {
  @Input()
  rating: number;
  @Input()
  ratingCount: number;

  constructor() {}

  getIcon(index) {
    if (index <= this.rating) return 'star';
    else if (index - this.rating > 0 && index - this.rating < 1) return 'star-half';
    else return 'star-outline';
  }
}
