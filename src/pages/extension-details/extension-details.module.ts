import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ExtensionDetailsPage } from './extension-details';
import { StarRating } from './rating/rating.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ExtensionDetailsPage, StarRating],
  imports: [IonicPageModule.forChild(ExtensionDetailsPage), TranslateModule.forChild()]
})
export class ExtensionDetailsPageModule {}
