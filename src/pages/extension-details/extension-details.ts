import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Slides } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import marked from 'marked';
import { MarketplaceProvider, FavoritesProvider, ToastProvider } from '../../providers';
import { Extension } from '../../models/extension.model';

@IonicPage()
@Component({
  selector: 'page-extension-details',
  templateUrl: 'extension-details.html'
})
export class ExtensionDetailsPage {
  @ViewChild('SwipeSlides') SwipeSlides: Slides;
  tabs = ['T1', 'T2', 'T3', 'T4'];
  segmentIndicator: HTMLElement = null;
  extension: Extension;
  isFavorite = false;
  target: string;
  changelog: any;
  changelogRaw: any;
  readme: any;
  icon: any;

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private mp: MarketplaceProvider,
    private favs: FavoritesProvider,
    private toast: ToastProvider,
    private popoverCtrl: PopoverController,
    private sanitizer: DomSanitizer
  ) {
    // get extension details
    this.extension = this.navParams.get('extension') || [];
    this.target = this.extension.installTarget || '';
    console.log(this.extension);

    this.getChangelog();
    this.getReadme();
    //check if extension is favorite
    this.favs.isFavorite(this.extension.extId).then(isFav => {
      this.isFavorite = isFav;
    });
  }

  ionViewDidEnter(): void {
    this.segmentIndicator = document.getElementById('indicator');
    // this.selectTab(2);
  }

  private getChangelog(): void {
    if (this.extension.changelog)
      this.mp.getMarkdown(this.extension.changelog.source).subscribe(res => {
        if (res) this.changelog = this.sanitizer.bypassSecurityTrustHtml(marked(res));
      });
  }

  private getReadme(): void {
    if (this.extension.readme)
      this.mp.getMarkdown(this.extension.readme.source).subscribe(res => {
        if (res) this.readme = this.sanitizer.bypassSecurityTrustHtml(marked(res));
        else this.readme = 'Not available.';
      });
  }
  private watchExtension(): void {
    this.favs.watchExtension(this.extension.extId).then(() => {
      this.toast.presentToast('Extension added to watchlist!', 3000, 'bottom');
      this.isFavorite = true;
    });
  }

  private unwatchExtension(): void {
    this.favs.unwatchExtension(this.extension.extId).then(() => {
      this.toast.presentToast('Extension removed from watchlist!', 3000, 'bottom');
      this.isFavorite = false;
    });
  }

  public selectTab(index): void {
    this.segmentIndicator.style.webkitTransform = `translate3d(${100 * index}%,0,0)`;
    this.SwipeSlides.slideTo(index, 500);
  }

  public updateIndicatorPosition(): void {
    if (this.SwipeSlides.length() > this.SwipeSlides.getActiveIndex())
      this.segmentIndicator.style.webkitTransform = `translate3d(${this.SwipeSlides.getActiveIndex() * 100}%,0,0)`;
  }

  public animateIndicator($event): void {
    if (this.segmentIndicator)
      this.segmentIndicator.style.webkitTransform = `translate3d(${$event.progress * (this.SwipeSlides.length() - 1) * 100}%,0,0)`;
  }

  public presentMenu(event): void {
    let menu = this.popoverCtrl.create('PopoverPage', {
      isFav: this.isFavorite,
      url: this.extension.url,
      target: this.extension.installTarget,
      name: this.extension.name,
      file: this.extension.icon
    });
    menu.present({
      ev: event
    });
    menu.onWillDismiss(res => {
      switch (res) {
        case 'watch': {
          this.watchExtension();
          break;
        }
        case 'unwatch': {
          this.unwatchExtension();
          break;
        }
      }
    });
  }
}
