import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExploreAllPage } from './explore-all';

@NgModule({
  declarations: [
    ExploreAllPage,
  ],
  imports: [
    IonicPageModule.forChild(ExploreAllPage),
  ],
})
export class ExploreAllPageModule {}
