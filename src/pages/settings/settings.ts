import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Settings, ThemeProvider, LanguageProvider } from '../../providers';
import { AppTheme, AppLanguage } from '../../models/interfaces';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  // settings object
  options: any;
  settingsReady = false;
  form: FormGroup;
  languageList: AppLanguage[] = this.lp.languages;
  themeList: AppTheme[] = this.tp.themes;

  themeSettings = {
    page: 'theme',
    pageTitleKey: 'PAGE_SETTINGS.THEME_TITLE'
  };
  themePage: any = SettingsPage;

  // current active page
  page: string = 'main';
  pageTitleKey: string = 'PAGE_SETTINGS.MAIN_TITLE';

  constructor(
    public navCtrl: NavController,
    public settings: Settings,
    private tp: ThemeProvider,
    private lp: LanguageProvider,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public translate: TranslateService
  ) {
    this.initSettings();
  }

  private initSettings(): void {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});

    this.page = this.navParams.get('page') || this.page;
    this.pageTitleKey = this.navParams.get('pageTitleKey') || this.pageTitleKey;

    this.settings.loadSettings().then(() => {
      this.settingsReady = true;
      this.options = this.settings.allSettings;
      this.buildForm();
    });
  }

  private buildForm(): void {
    let group: any = {
      appLanguage: [this.options.appLanguage],
      prefetchExtensions: [this.options.prefetchExtensions]
    };

    switch (this.page) {
      case 'main':
        break;
      case 'theme':
        group = {
          appTheme: [this.options.appTheme]
        };
        break;
    }
    this.form = this.formBuilder.group(group);

    // Watch the form for changes, and
    this.form.valueChanges.subscribe(v => {
      console.log(v);
      this.settings.merge(this.form.value);
    });
  }

  public changeTheme(theme: string): void {
    this.form.controls['appTheme'].setValue(theme);
    this.tp.setTheme(theme);
  }

  public changeLanguage(lang: string): void {
    this.lp.appLanguage.next(lang);
  }
}
