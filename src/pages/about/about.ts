import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { SocialProvider } from '../../providers/social/social';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  constructor(public navCtrl: NavController, private social: SocialProvider) {}

  public goToDonate(): void {
    this.navCtrl.setRoot('DonatePage');
  }

  public openWebpage(page: string): void {
    this.social.openWebPage(page);
  }

  public share(): void {
    this.social.shareApp();
  }

  public rate(): void {
    // TODO
    // window.open(‘market://developer?id=YOUR+NAME’, ‘_system’, ‘location=yes’);
  }

  public contact(): void {
    this.social.contactAuthor();
  }
}
