import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';

import { Settings } from '../../providers';
import { TUTORIAL_SLIDES } from '../../app/app.config';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides = TUTORIAL_SLIDES;
  showSkip = true;

  constructor(
    public navCtrl: NavController,
    public menu: MenuController,
    private settings: Settings,
    public platform: Platform
  ) {}

  startApp() {
    this.navCtrl
      .setRoot('HomePage', {}, { animate: true, direction: 'forward' })
      // .then(() => this.settings.setValue('userDidTutorial', true));
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }
}
