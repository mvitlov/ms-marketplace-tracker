import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DONATIONS } from '../../app/app.config';

@IonicPage()
@Component({
  selector: 'page-donate',
  templateUrl: 'donate.html'
})
export class DonatePage {
  donationList = DONATIONS;
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonatePage');
  }
}
