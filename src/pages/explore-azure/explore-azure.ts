import { Component } from '@angular/core';
import { IonicPage, NavController, PopoverController, NavParams } from 'ionic-angular';
import { MarketplaceProvider, Settings } from '../../providers';
import { AZURE_TOOLBAR } from '../../app/app.config';

@IonicPage()
@Component({
  selector: 'page-explore-azure',
  templateUrl: 'explore-azure.html'
})
export class ExploreAzurePage {
  public viewData = {};
  public toolbarItems = AZURE_TOOLBAR;
  public loading = true;
  public trending = 'TrendingDaily';
  public currentView = 'Explore';
  public orderBy = 4; // 4:"downloads" - popover menu parameter
  private prefetch = false;
  // data: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private popover: PopoverController,
    private mp: MarketplaceProvider,
    private settings: Settings
  ) {
    this.mp.getAzureExtensions().subscribe(
      result => {
        result['epc'].forEach(element => {
          this.viewData[element.cn] = element.e;
        });
        console.log(this.viewData);
      },
      err => {
        console.log(err);
        this.loading = false;
      },
      () => {
        setTimeout(() => {
          this.loading = false;
        });
      }
    );

    this.settings.getValue('prefetchExtensions').then(res => {
      if (res) {
        this.prefetchExtensions();
        this.prefetch = true;
      }
    });
  }

  public onTrendingChange(event: string) {
    this.trending = event;
  }

  public checkActiveView(button) {
    return button.name === this.currentView;
  }

  public switchView(button) {
    this.currentView = button.name;
    if (!this.prefetch && button.name != 'Explore') {
      this.loadExtensionsCategory(button.name);
    }
  }

  private prefetchExtensions() {
    AZURE_TOOLBAR.slice(1).forEach(el => {
      this.mp.getAzureExtensions(el.name).subscribe(res => {
        console.log(res);
        this.viewData[el.name] = res;
      });
    });
  }

  private loadExtensionsCategory(name: string) {
    this.loading = true;
    this.mp.getAzureExtensions(name, 1, this.orderBy).subscribe(
      res => {
        this.viewData[name] = res;
      },
      err => {
        console.log(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  /* PRESENT 'ORDER BY' POPOVER MENU */
  public presentMenu(event: any): void {
    const menu = this.popover.create('OrderByComponent', { orderBy: this.orderBy });

    menu.present({ ev: event });

    menu.onWillDismiss((order: number) => {
      if (order && order !== this.orderBy) {
        this.orderBy = order;
        this.loadExtensionsCategory(this.currentView);
      }
    });
  }
}
