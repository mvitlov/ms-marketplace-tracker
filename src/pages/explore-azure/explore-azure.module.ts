import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ExploreAzurePage } from './explore-azure';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [ExploreAzurePage],
  imports: [IonicPageModule.forChild(ExploreAzurePage), TranslateModule.forChild(), ComponentsModule]
})
export class ExploreAzurePageModule {}
