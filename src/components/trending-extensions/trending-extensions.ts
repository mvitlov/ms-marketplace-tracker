import { Component, Input, Output, EventEmitter } from '@angular/core';
import { App, LoadingController, Loading, ModalController } from 'ionic-angular';
import { MarketplaceProvider } from '../../providers';
/**
 * Generated class for the TrendingExtensionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'trending-extensions',
  templateUrl: 'trending-extensions.html'
})
export class TrendingExtensionsComponent {
  @Input() extensions: any;
  @Output() trendingChange = new EventEmitter<string>();
  public trending = 'TrendingDaily';
  loadingNotif: Loading;
  constructor(
    private app: App,
    private loading: LoadingController,
    private modal: ModalController,
    private mp: MarketplaceProvider
  ) {}

  public onExtensionDetails(ext): void {
    this.showLoading();
    this.mp.searchByName(ext.t).subscribe(res => {
      this.app.getRootNav().push('ExtensionDetailsPage', { extension: res });
    });
  }
  public onViewAll(title, data): void {
    console.log(data);

    let viewAllModal = this.modal.create('ExploreAllPage', { title: title, data: data });
    viewAllModal.present();
  }

  private showLoading(): void {
    this.loadingNotif = this.loading.create({
      content: 'Loading extension details...',
      dismissOnPageChange: true
    });

    this.loadingNotif.present();
  }
  public trendChange(event: string) {
    this.trendingChange.emit(event);
    this.trending = event;
  }
}
