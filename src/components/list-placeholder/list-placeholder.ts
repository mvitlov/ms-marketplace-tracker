import { Component } from '@angular/core';

/**
 * Generated class for the ExplorePlaceholderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-placeholder',
  templateUrl: 'list-placeholder.html'
})
export class ListPlaceholderComponent {
  text: string;

  constructor() {
    console.log('Hello ExplorePlaceholderComponent Component');
    this.text = 'Hello World';
  }
}
