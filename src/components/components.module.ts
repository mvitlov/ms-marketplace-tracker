import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ExplorePlaceholderComponent } from './explore-placeholder/explore-placeholder';
import { ListPlaceholderComponent } from './list-placeholder/list-placeholder';
import { FeaturedExtensionsComponent } from './featured-extensions/featured-extensions';
import { TrendingExtensionsComponent } from './trending-extensions/trending-extensions';
import { MostPopularExtensionsComponent } from './most-popular-extensions/most-popular-extensions';
import { RecentlyAddedExtensionsComponent } from './recently-added-extensions/recently-added-extensions';
import { HighestRatedExtensionsComponent } from './highest-rated-extensions/highest-rated-extensions';
import { ExploreListComponent } from './explore-list/explore-list';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    ExplorePlaceholderComponent,
    ListPlaceholderComponent,
    FeaturedExtensionsComponent,
    TrendingExtensionsComponent,
    MostPopularExtensionsComponent,
    RecentlyAddedExtensionsComponent,
    HighestRatedExtensionsComponent,
    ExploreListComponent
  ],
  imports: [IonicModule, PipesModule],
  exports: [
    ExplorePlaceholderComponent,
    ListPlaceholderComponent,
    FeaturedExtensionsComponent,
    TrendingExtensionsComponent,
    MostPopularExtensionsComponent,
    RecentlyAddedExtensionsComponent,
    HighestRatedExtensionsComponent,
    ExploreListComponent
  ]
})
export class ComponentsModule {}
