import { Component } from '@angular/core';

/**
 * Generated class for the ExplorePlaceholderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'explore-placeholder',
  templateUrl: 'explore-placeholder.html'
})
export class ExplorePlaceholderComponent {

  text: string;

  constructor() {
    console.log('Hello ExplorePlaceholderComponent Component');
    this.text = 'Hello World';
  }

}
