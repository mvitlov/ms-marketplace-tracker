import { Component, Input } from '@angular/core';
import { App, LoadingController, Loading } from 'ionic-angular';
import { MarketplaceProvider } from '../../providers';

/**
 * Generated class for the FeaturedExtensionsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'featured-extensions',
  templateUrl: 'featured-extensions.html'
})
export class FeaturedExtensionsComponent {
  @Input() extension: any;
  loadingNotif: Loading;
  data;
  constructor(private app: App, private loading: LoadingController, private mp: MarketplaceProvider) {}

  public onExtensionDetails(ext) {
    this.showLoading();
    this.mp.searchByName(ext.t).subscribe(res => {
      this.app.getRootNav().push('ExtensionDetailsPage', { extension: res });
    });
  }
  private showLoading(): void {
    this.loadingNotif = this.loading.create({
      content: 'Loading extension details...',
      dismissOnPageChange: true
    });

    this.loadingNotif.present();
  }
}
