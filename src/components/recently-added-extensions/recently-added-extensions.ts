import { Component, Input } from '@angular/core';
import { App, LoadingController, Loading, ModalController } from 'ionic-angular';
import { MarketplaceProvider } from '../../providers';

@Component({
  selector: 'recently-added-extensions',
  templateUrl: 'recently-added-extensions.html'
})
export class RecentlyAddedExtensionsComponent {
  @Input() extensions: any;
  loadingNotif: Loading;

  constructor(
    private app: App,
    private loading: LoadingController,
    private modal: ModalController,
    private mp: MarketplaceProvider
  ) {}

  public onExtensionDetails(ext): void {
    this.showLoading();
    this.mp.searchByName(ext.t).subscribe(res => {
      this.app.getRootNav().push('ExtensionDetailsPage', { extension: res });
    });
  }
  private showLoading(): void {
    this.loadingNotif = this.loading.create({
      content: 'Loading extension details...',
      dismissOnPageChange: true
    });

    this.loadingNotif.present();
  }

  public onViewAll(data): void {
    let viewAllModal = this.modal.create('ExploreAllPage', { title: 'MostPopular', data: data });
    viewAllModal.present();
  }
}
