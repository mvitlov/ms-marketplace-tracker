import { Component, Input } from '@angular/core';
import { MarketplaceProvider } from '../../providers/marketplace/marketplace';

/**
 * Generated class for the ExploreListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'explore-list',
  templateUrl: 'explore-list.html'
})
export class ExploreListComponent {
  @Input() area: string;
  @Input() orderBy: number;
  @Input() extensions: any[];
  @Input() viewTitle: string;
  private paging = {
    // VISUAL STUDIO CATEGORIES
    'Tools/Coding': 1,
    'Controls/Framework & Libraries': 1,
    'Tools/Programming Languages': 1,
    'Controls/Windows Forms': 1,
    'Tools/Team Development': 1,
    // VS CODE CATEGORIES
    Azure: 1,
    Debuggers: 1,
    'Extension Packs': 1,
    Formatters: 1,
    Keymaps: 1,
    'Language Packs': 1,
    Linters: 1,
    'Programming Languages': 1,
    'SCM Providers': 1,
    Snippets: 1,
    Themes: 1,
    Other: 1,
    // AZURE CATEGORIES
    'Azure Artifacts': 1,
    'Azure Boards': 1,
    'Azure Pipelines': 1,
    'Azure Repos': 1,
    'Azure Test Plans': 1
  };

  constructor(private mp: MarketplaceProvider) {}

  public getMoreResults(event) {
    this.paging[this.viewTitle] += 1; // increase page number

    switch (this.area) {
      case 'studio': {
        return this.mp.getVisualStudioExtensions(this.viewTitle, this.paging[this.viewTitle], this.orderBy).subscribe(res => {
          this.extensions.push(...res);
          event.complete();
        });
      }
      case 'code': {
        return this.mp.getVsCodeExtensions(this.viewTitle, this.paging[this.viewTitle]).subscribe(res => {
          this.extensions.push(...res);
          event.complete();
        });
      }

      case 'azure': {
        return this.mp.getAzureExtensions(this.viewTitle, this.paging[this.viewTitle]).subscribe(res => {
          this.extensions.push(...res);
          event.complete();
        });
      }
    }
  }
}
