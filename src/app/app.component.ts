import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { Settings, FavoritesProvider, ThemeProvider, LanguageProvider } from '../providers/';
import { MENU_PAGES } from './app.config';
import { SocialProvider } from '../providers/social/social';

@Component({
  templateUrl: './app.component.html'
})
export class MyApp {
  rootPage: any;
  appName: string = 'VS Extension Tracker';
  appTheme: any;
  appLanguage: any;
  pages = MENU_PAGES;
  watchCount: any;
  activePage: any;

  @ViewChild(Nav) nav: Nav;

  constructor(
    private translate: TranslateService,
    private fp: FavoritesProvider,
    public tp: ThemeProvider,
    platform: Platform,
    private settings: Settings,
    private language: LanguageProvider,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private social: SocialProvider
  ) {
    platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.setRootPage();
    this.initSettings();
    this.fp.favoriteCount.subscribe(res => {
      this.watchCount = res;
    });
  }

  // initTranslate() {
  //   this.settings.getValue('appLanguage').then(res => {
  //     res ? this.translate.use(res) : this.translate.use('en');
  //   });

  //   // this.translate.setDefaultLang('en');
  //   // this.translate.addLangs(['hr']);
  //   // const browserLang = this.translate.getBrowserLang();

  //   // if (browserLang) {
  //   //   if (browserLang === 'zh') {
  //   //     const browserCultureLang = this.translate.getBrowserCultureLang();

  //   //     if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
  //   //       this.translate.use('zh-cmn-Hans');
  //   //     } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
  //   //       this.translate.use('zh-cmn-Hant');
  //   //     }
  //   //   } else {
  //   //     this.translate.use(this.translate.getBrowserLang());
  //   //   }
  //   // } else {
  //   //   this.translate.use('en'); // Set your language here
  //   // }

  //   // this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
  //   //   this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
  //   // });
  // }

  private initSettings(): void {
    this.language.appLanguage.subscribe(lang => this.translate.use(lang));
  }

  private setRootPage(): void {
    this.rootPage = 'ExplorePage';
    return;
    /*     this.settings.getValue('userDidTutorial').then(didTutorial => {
      if (didTutorial) {
        this.rootPage = 'HomePage';
        this.activePage = this.pages.find(page => page['component'] === this.rootPage);
      } else {
        this.rootPage = 'TutorialPage';
      }
    }); */
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  checkActivePage(page) {
    return page === this.activePage;
  }
  public parseAction(method: string): void {
    switch (method) {
      case 'contact': {
        this.contact();
        break;
      }
      case 'share': {
        this.share();
        break;
      }
      case 'rate': {
        this.rate();
        break;
      }
    }
  }
  public share(): void {
    this.social.shareApp();
  }
  public rate(): void {
    // TODO
  }
  public contact(): void {
    this.social.contactAuthor();
  }
}
