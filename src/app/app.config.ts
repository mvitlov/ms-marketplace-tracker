/* SIDE MENU PAGES LIST */
export const MENU_PAGES = [
  { type: 'divider', title: 'Main' },
  { type: 'menuItem', title: 'Home', component: 'HomePage', icon: 'home' },
  { type: 'menuItem', title: 'Search', component: 'SearchPage', icon: 'search' },
  { type: 'menuItem', title: 'Explore', component: 'ExplorePage', icon: 'compass' },
  { type: 'divider', title: 'Settings' },
  { type: 'menuItem', title: 'Settings', component: 'SettingsPage', icon: 'settings' },
  { type: 'divider', title: 'Misc' },
  { type: 'command', title: 'Share', action: 'share', icon: 'share' },
  // TODO: RATE APP
  // { type: 'command', title: 'Rate App', action: 'rate', icon: 'star' },
  { type: 'command', title: 'Contact', action: 'contact', icon: 'mail' },
  // TODO: DONATE
  // { type: 'menuItem', title: 'Donate', component: 'DonatePage', icon: 'card' },
  { type: 'menuItem', title: 'About', component: 'AboutPage', icon: 'information-circle' }
  // { type: 'menuItem', title: 'Tutorial', component: 'TutorialPage' },
  // { type: 'menuItem', title: 'Welcome', component: 'WelcomePage' },
  // { type: 'menuItem', title: 'Tabs', component: 'TabsPage' }
  // { type: 'menuItem', title: 'Cards', component: 'CardsPage' },
  // { type: 'menuItem', title: 'Content', component: 'ContentPage' },
  // { type: 'menuItem', title: 'Login', component: 'LoginPage' },
  // { type: 'menuItem', title: 'Signup', component: 'SignupPage' },
  // { type: 'menuItem', title: 'Master Detail', component: 'ListMasterPage' },
  // { type: 'menuItem', title: 'Menu', component: 'MenuPage' },
];

/* VS MARKETPLACE CONSTANTS */
export const API_VERSION = '5.0-preview.1';
export const BASE_URL = 'https://marketplace.visualstudio.com/_apis/public/gallery/extensionquery';
export const HEADERS = {
  Accept: `application/json;api-version=${API_VERSION};excludeUrls=true`,
  'Content-Type': 'application/json'
};
export const VS_URL = 'https://marketplace.visualstudio.com/getextensionspercategory?Product=vs&RemoveFirstSetCategories=true';
export const CODE_URL = 'https://marketplace.visualstudio.com/getextensionspercategory?Product=vscode&RemoveFirstSetCategories=false';
export const AZURE_URL = 'https://marketplace.visualstudio.com/getextensionspercategory?Product=vsts&RemoveFirstSetCategories=false';

export const extensionUrl = 'https://marketplace.visualstudio.com/items?itemName=';

export const STUDIO_TOOLBAR = [
  { name: 'Explore', icon: 'compass' },
  { name: 'Tools/Coding', icon: 'code' },
  { name: 'Controls/Framework & Libraries', icon: 'build' },
  { name: 'Tools/Programming Languages', icon: 'create' },
  { name: 'Controls/Windows Forms', icon: 'browsers' },
  { name: 'Tools/Team Development', icon: 'contacts' }
];

export const CODE_TOOLBAR = [
  { name: 'Explore', icon: 'compass' },
  { name: 'Azure', icon: 'cloud' },
  { name: 'Debuggers', icon: 'bug' },
  { name: 'Extension Packs', icon: 'albums' },
  { name: 'Formatters', icon: 'hammer' },
  { name: 'Keymaps', icon: 'keypad' },
  { name: 'Language Packs', icon: 'globe' },
  { name: 'Linters', icon: 'checkmark-circle' },
  { name: 'Programming Languages', icon: 'create' },
  { name: 'SCM Providers', icon: 'git-branch' },
  { name: 'Snippets', icon: 'clipboard' },
  { name: 'Themes', icon: 'color-palette' },
  { name: 'Other', icon: 'help' }
];

export const AZURE_TOOLBAR = [
  { name: 'Explore', icon: 'compass' },
  { name: 'Azure Artifacts', icon: 'cube' },
  { name: 'Azure Boards', icon: 'albums' },
  { name: 'Azure Pipelines', icon: 'construct' },
  { name: 'Azure Repos', icon: 'git-branch' },
  { name: 'Azure Test Plans', icon: 'flask' }
];

export function studioFilters(section: string) {
  return {
    criteria: [
      {
        filterType: 10,
        value: `target:"Microsoft.VisualStudio.VSWinDesktopExpress" target:"Microsoft.VisualStudio.VSWinExpress" target:"Microsoft.VisualStudio.VWDExpress" target:"Microsoft.VisualStudio.Community" target:"Microsoft.VisualStudio.Pro" target:"Microsoft.VisualStudio.Enterprise" target:"Microsoft.VisualStudio.IntegratedShell" target:"Microsoft.VisualStudio.Isolated" target:"Microsoft.VisualStudio.Test" target:"Microsoft.VisualStudio.Ultimate" target:"Microsoft.VisualStudio.Premium" target:"Microsoft.VisualStudio.VST_All" target:"Microsoft.VisualStudio.VSLS" target:"Microsoft.VisualStudio.VPDExpress"`
      },
      { filterType: 12, value: '37888' },
      { filterType: 5, value: section }
    ],
    direction: 2,
    pageNumber: 1,
    pageSize: 54,
    sortBy: 4,
    sortOrder: 0
  };
}
export function codeFilters(section: string) {
  return {
    criteria: [
      {
        filterType: 10,
        value: `target:"Microsoft.VisualStudio.Code"`
      },
      { filterType: 12, value: '37888' },
      { filterType: 5, value: section }
    ],
    direction: 2,
    pageNumber: 1,
    pageSize: 54,
    sortBy: 4,
    sortOrder: 0
  };
}

export function azureFilters(section: string) {
  return {
    criteria: [
      {
        filterType: 10,
        value: `target:"Microsoft.VisualStudio.Services" target:"Microsoft.VisualStudio.Services.Integration" target:"Microsoft.VisualStudio.Services.Cloud" target:"Microsoft.TeamFoundation.Server" target:"Microsoft.TeamFoundation.Server.Integration" target:"Microsoft.VisualStudio.Services.Cloud.Integration" target:"Microsoft.VisualStudio.Services.Resource.Cloud"`
      },
      { filterType: 12, value: '37888' },
      { filterType: 5, value: section }
    ],
    direction: 2,
    pageNumber: 1,
    pageSize: 54,
    sortBy: 4,
    sortOrder: 0
  };
}

/* THEME PROVIDER CONSTANTS */
export const THEMES = [
  { title: 'Default Theme', theme: 'theme-default', color: '#2980b9' },
  { title: 'Red Theme', theme: 'theme-red', color: '#FF0000' },
  { title: 'Noir', theme: 'theme-noir', color: '#333333' },
  { title: 'Clover', theme: 'theme-clover', color: '#388E3C' },
  { title: 'Blueberry', theme: 'theme-blueberry', color: '#1e88e5' }
];

/* DONATION LIST CONSTANTS */
export const DONATIONS = [
  [
    { value: 1, icon: 'assets/img/donate/tuna.svg' },
    { value: 2, icon: 'assets/img/donate/hotdog.svg' },
    { value: 5, icon: 'assets/img/donate/hotdog-plus.svg' }
  ],
  [
    { value: 10, icon: 'assets/img/donate/burger.svg' },
    { value: 20, icon: 'assets/img/donate/burger-plus.svg' },
    { value: 50, icon: 'assets/img/donate/steak.svg' }
  ]
];

/* LANGUAGE PROVIDER CONSTANTS */
export const LANGUAGES = [{ short: 'hr', long: 'Hrvatski' }, { short: 'en', long: 'English' }];

/* TUTORIAL SLIDES CONSTANTS */
export const TUTORIAL_SLIDES = [
  {
    TITLE: 'Track Extensions',
    DESCRIPTION: "With <b>VS Extension Tracker</b> it's possible to have all of your VS Marketplace extensions in one place. ",
    image: './assets/img/tutorial/tutorial1.png'
  },
  {
    TITLE: 'Realtime Stats',
    DESCRIPTION:
      "Monitor your extension's statistics in <b>real time</b>. Check installs, trends & ratings and compare against other extensions.",
    image: './assets/img/tutorial/tutorial2.png'
  },
  {
    TITLE: 'Explore Marketplace',
    DESCRIPTION:
      'Looking for new trends?<br>Or maybe an inspiration for your next project? Explore the VS Marketplace to find something interesting.',
    image: './assets/img/tutorial/tutorial3.png'
  },
  {
    TITLE: 'Ready to start?',
    DESCRIPTION: "Let's go!",
    image: './assets/img/tutorial/tutorial4.png'
  }
];
