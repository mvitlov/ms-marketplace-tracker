// import { Injectable } from '@angular/core';
// import { Platform } from 'ionic-angular';
// import { Network } from '@ionic-native/network';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import { Subscription } from 'rxjs/Subscription';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/of';

// @Injectable()
// export class NetworkProvider {
//   private connected: BehaviorSubject<boolean> = new BehaviorSubject(false);
//   private subscribedToNetworkStatus: boolean = false;

//   constructor(public network: Network, private platform: Platform) {
//     if (this.network.type !== 'none') {
//       this.connected.next(true);
//     }
//   }

//   public setSubscriptions() {
//     if (!this.subscribedToNetworkStatus && this.platform.is('cordova')) {
//       this.subscribedToNetworkStatus = true;

//       this.network.onConnect().subscribe(() => {
//         this.connected.next(true);
//       });
//       this.network.onchange().subscribe(() => {});

//       this.network.onDisconnect().subscribe(() => {
//         this.connected.next(false);
//       });
//     }
//   }

//   public networkStatus(): BehaviorSubject<boolean> {
//     return this.connected;
//   }
// }
