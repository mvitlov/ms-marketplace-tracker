import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

/*
  Generated class for the ToastProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToastProvider {
  constructor(private toast: ToastController) {}

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toast.create({
      message: message,
      duration: duration,
      position: position,
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    toast.dismissAll();
    toast.present();
  }
}
