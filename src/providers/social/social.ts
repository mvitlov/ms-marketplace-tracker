import { Injectable } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Injectable()
export class SocialProvider {
  constructor(private share: SocialSharing, private iab: InAppBrowser) {}

  public shareApp(extOptions?: any): void {
    if (extOptions) {
      this.share.shareWithOptions(extOptions);
    } else {
      const appOptions = {
        message: 'Hey, check out this app for tracking your VS Marketplace Extensions.',
        subject: 'VS Extension Tracker',
        file: '../assets/img/appicon.png',
        url: 'https://www.website.com/foo/#bar?a=b',
        chooserTitle: 'Choose an app'
      };
      this.share.shareWithOptions(appOptions);
    }
  }

  public contactAuthor(): void {
    let emailOptions = 'mailto:marin.vitlov@gmail.com?Subject=Feedback%20on%20VS%20Extension%20Tracker%20App';
    this.iab.create(emailOptions, '_system');
  }

  public openWebPage(page: string): void {
    this.iab.create(page, '_system');
  }
}
