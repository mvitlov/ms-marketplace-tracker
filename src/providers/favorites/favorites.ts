import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FavoritesProvider {
  private storageKey = 'fav-ext';
  public favoriteCount = new Subject();

  constructor(private storage: Storage) {
    this.getStoredExtensions().then(result => {
      result ? this.favoriteCount.next(result.length) : this.favoriteCount.next(0);
    });
  }

  /* track extension (save to storage) */
  watchExtension(id): Promise<any> {
    return this.getStoredExtensions().then(result => {
      if (result) {
        result.push(id);
        this.favoriteCount.next(result.length);
        return this.storage.set(this.storageKey, result);
      } else {
        return this.storage.set(this.storageKey, [id]).then(result => {
          this.favoriteCount.next(result.length);
        });
      }
    });
  }

  /* untrack extension (remove from storate) */
  unwatchExtension(id): Promise<any> {
    return this.getStoredExtensions().then(result => {
      if (result) {
        let index = result.indexOf(id);
        result.splice(index, 1);
        this.favoriteCount.next(result.length);
        return this.storage.set(this.storageKey, result).then(result => {
          this.favoriteCount.next(result.length);
        });
      }
    });
  }

  /* returns all tracked extensions */
  public getStoredExtensions(): Promise<any> {
    return this.storage.get(this.storageKey);
  }

  /* is extension tracked */
  public isFavorite(id: string): Promise<boolean> {
    return this.getStoredExtensions().then(result => {
      return result && result.indexOf(id) !== -1;
    });
  }
}
