import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Criterion, Filter, SearchRequest } from '../../models';
import {
  API_VERSION,
  BASE_URL,
  HEADERS,
  VS_URL,
  CODE_URL,
  AZURE_URL,
  studioFilters,
  codeFilters,
  azureFilters
} from '../../app/app.config';
import { Observable } from 'rxjs/Observable';
import { Extension } from '../../models/extension.model';
import { map } from 'rxjs/operators';

@Injectable()
export class MarketplaceProvider {
  constructor(private http: HttpClient) {}

  public searchExtensions(searchString: string): Observable<any> {
    //10 - searchString
    const criteria = [new Criterion(10, searchString)];
    const filter = [new Filter(criteria)];
    const body = new SearchRequest(filter);
    const headers = new HttpHeaders(HEADERS);

    return this.http.post(BASE_URL, body, { headers: headers });
  }

  public getMarkdown(url: string): Observable<any> {
    return this.http.get(url, { responseType: 'text' });
  }

  public getExtensionById(extId: string): Observable<Extension> {
    // 4 - extensionID
    const criteria = [new Criterion(4, extId)];
    const filter = [new Filter(criteria)];
    const body = new SearchRequest(filter);
    const headers = new HttpHeaders(HEADERS);

    return this.http.post(BASE_URL, body, { headers: headers }).pipe(map((data: any) => new Extension(data.results[0].extensions[0])));
  }

  public searchByName(name: string) {
    // 2 - extensionDisplayName
    const criteria = [new Criterion(10, name)];
    const filter = [new Filter(criteria, 1)];
    const body = new SearchRequest(filter);
    const headers = new HttpHeaders(HEADERS);

    return this.http.post(BASE_URL, body, { headers: headers }).pipe(map((data: any) => new Extension(data.results[0].extensions[0])));
  }

  public getVisualStudioExtensions(section?: any, pageNumber?: number, sortBy?: number): Observable<any> {
    const headers = new HttpHeaders(HEADERS);
    if (section) {
      return this.http.post(BASE_URL, this.buildBody(studioFilters(section), pageNumber, sortBy), { headers: headers }).pipe(
        map((data: any) => {
          return data.results[0].extensions.map(extension => new Extension(extension));
        })
      );
    } else {
      return this.http.get(VS_URL);
    }
  }
  public getVsCodeExtensions(section?: any, pageNumber?: number, sortBy?: number): Observable<any> {
    const headers = new HttpHeaders(HEADERS);
    if (section) {
      return this.http.post(BASE_URL, this.buildBody(codeFilters(section), pageNumber, sortBy), { headers: headers }).pipe(
        map((data: any) => {
          return data.results[0].extensions.map(extension => new Extension(extension));
        })
      );
    } else {
      return this.http.get(CODE_URL);
    }
  }
  public getAzureExtensions(section?: any, pageNumber?: number, sortBy?: number): Observable<any> {
    const headers = new HttpHeaders(HEADERS);
    if (section) {
      return this.http.post(BASE_URL, this.buildBody(azureFilters(section), pageNumber, sortBy), { headers: headers }).pipe(
        map((data: any) => {
          return data.results[0].extensions.map(extension => new Extension(extension));
        })
      );
    } else {
      return this.http.get(AZURE_URL);
    }
  }

  private buildBody(data: any, pageNumber?: number, sortBy?: number) {
    const criteria = [];
    data.criteria.forEach(el => {
      criteria.push(new Criterion(el.filterType, el.value));
    });
    const filter = [
      new Filter(criteria, data.direction, pageNumber || data.pageNumber, data.pageSize, sortBy || data.sortBy, data.sortOrder)
    ];
    return new SearchRequest(filter);
  }
}
