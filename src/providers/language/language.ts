import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { LANGUAGES } from '../../app/app.config';
import { AppLanguage } from '../../models/interfaces';
import { Settings } from '../';

@Injectable()
export class LanguageProvider {
  public appLanguage = new Subject<string>();
  private languagesList: AppLanguage[] = LANGUAGES;

  constructor(private settings: Settings) {
    settings.getValue('appLanguage').then(lang => {
      lang ? this.appLanguage.next(lang) : this.appLanguage.next('en');
    });
  }

  get languages() {
    return this.languagesList;
  }
}
