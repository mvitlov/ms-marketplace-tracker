import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { THEMES } from '../../app/app.config';
import { AppTheme } from '../../models/interfaces';
import { Settings } from '../';

@Injectable()
export class ThemeProvider {
  public appTheme = new Subject<string>();
  private themeList: AppTheme[] = THEMES;

  constructor(private settings: Settings) {
    this.settings.getValue('appTheme').then(theme => {
      theme ? this.appTheme.next(theme) : this.appTheme.next('theme-default');
    });
  }

  public setTheme(themeName: string) {
    this.appTheme.next(themeName);
  }

  get themes() {
    return this.themeList;
  }
}
