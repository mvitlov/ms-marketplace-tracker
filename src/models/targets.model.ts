export let Azure = [
  'Microsoft.VisualStudio.Services',
  'Microsoft.VisualStudio.Services.Integration',
  'Microsoft.VisualStudio.Services.Cloud',
  'Microsoft.TeamFoundation.Server',
  'Microsoft.TeamFoundation.Server.Integration',
  'Microsoft.VisualStudio.Services.Cloud.Integration',
  'Microsoft.VisualStudio.Services.Resource.Cloud'
];

export let VisualStudio = [
  'Microsoft.VisualStudio.VSWinDesktopExpress',
  'Microsoft.VisualStudio.VSWinExpress',
  'Microsoft.VisualStudio.VWDExpress',
  'Microsoft.VisualStudio.Community',
  'Microsoft.VisualStudio.Pro',
  'Microsoft.VisualStudio.Enterprise',
  'Microsoft.VisualStudio.IntegratedShell',
  'Microsoft.VisualStudio.Isolated',
  'Microsoft.VisualStudio.Test',
  'Microsoft.VisualStudio.Ultimate',
  'Microsoft.VisualStudio.Premium',
  'Microsoft.VisualStudio.VST_All',
  'Microsoft.VisualStudio.VSLS',
  'Microsoft.VisualStudio.VPDExpress'
];

export let Code = ['Microsoft.VisualStudio.Code'];
