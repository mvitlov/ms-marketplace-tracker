import { Filter } from './filter';

export class SearchRequest {
  private assetTypes: Array<string> = null;
  private filters: Array<Filter> = null;
  private flags: number = 870;

  constructor(filters: Array<Filter>) {
    this.filters = filters;
  }

  public SearchRequest(list: Array<Filter>): void {
    this.filters = list;
  }

  public getFilters(): Array<Filter> {
    return this.filters;
  }

  public setFilters(list: Array<Filter>): void {
    this.filters = list;
  }

  public getAssetTypes(): Array<string> {
    return this.assetTypes;
  }

  public setAssetTypes(list: Array<string>): void {
    this.assetTypes = list;
  }

  public getFlags(): number {
    return this.flags;
  }

  public setFlags(i: number): void {
    this.flags = i;
  }
}
