import { Publisher, Statistic, Version, InstallationTarget } from './interfaces';
import { VisualStudio, Code, Azure } from './targets.model';

export class Extension {
  private categories: string[];
  private deploymentType: number;
  private displayName: string;
  private extensionId: string;
  private extensionName: string;
  private flags: string;
  private installationTargets: Array<InstallationTarget>;
  private lastUpdated: string;
  private publishedDate: string;
  private publisher: Publisher;
  private releaseDate: string;
  private shortDescription: string;
  private statistics: Array<Statistic>;
  private tags: string[];
  private versions: Array<Version>;

  constructor(extension: any) {
    Object.assign(this, extension);
  }

  public get url(): string {
    return `${this.publisher.publisherName}.${this.extensionName}`;
  }

  /* get extension name */
  public get name(): string {
    return this.displayName;
  }

  /* get extension name */
  public get extId(): string {
    return this.extensionId;
  }

  public get firstRelease(): string {
    return this.releaseDate;
  }

  public get lastUpdate(): string {
    return this.lastUpdated;
  }

  public get description(): string {
    return this.shortDescription;
  }

  public get version(): string {
    return this.versions[0].version;
  }
  public get tagsArray(): string[] {
    return this.tags;
  }
  public get categoriesArray(): string[] {
    return this.categories;
  }

  public getStat(statName: string): number {
    let value = 0;
    this.statistics.forEach(stat => {
      if (stat.statisticName.indexOf(statName) > -1) value = stat.value;
    });
    // return +value.toFixed(2);
    return value;
  }

  public get changelog() {
    return this.versions[0].files.find(file => file.assetType.indexOf('Changelog') > -1);
  }
  public get readme() {
    return this.versions[0].files.find(file => file.assetType.indexOf('Details') > -1);
  }

  public get uniq_id(): string {
    return this.extensionName;
  }

  public get publisherName(): string {
    return this.publisher.displayName;
  }

  /* get installation target: VS,Code or Azure */
  public get installTarget(): string {
    let targetArray = this.installationTargets.map(el => el.target);
    if (this.findTarget(targetArray, VisualStudio)) {
      return 'vs';
    } else if (this.findTarget(targetArray, Azure)) {
      return 'azure';
    } else if (this.findTarget(targetArray, Code)) {
      return 'code';
    } else {
      console.log('target not found');
      return 'unknown';
    }
  }

  /* compare arrays */
  private findTarget(targetArray: string[], searchArray: string[]): boolean {
    let found = false;
    for (let i = 0, j = targetArray.length; !found && i < j; i++) {
      if (searchArray.indexOf(targetArray[i]) > -1) {
        found = true;
        break;
      }
    }
    return found;
  }

  /* get extension icon url or return default icon if not found */
  public get icon(): string {
    const result = this.versions[0].files.find(file => file.assetType === 'Microsoft.VisualStudio.Services.Icons.Default');
    return result ? result.source : 'assets/img/default_ext_icon.png';
  }

  /* get total extension installs */
  public get totalInstalls(): number {
    let installs: number = 0;
    let targetArray = ['install', 'updateCount', 'migratedInstallCount', 'onpremDownloads'];
    this.statistics.forEach(el => {
      if (targetArray.indexOf(el.statisticName) > -1) {
        installs += el.value;
      }
    });
    return installs;
  }

  /* get extension rating score */
  public get rating(): number {
    let rating = 0;
    this.statistics.forEach(stat => {
      if (stat.statisticName === 'averagerating') {
        rating = stat.value;
      }
    });
    return Number(+rating.toFixed(2));
  }
}
