export class Criterion {
  private filterType: number;
  private value: string;

  constructor(filterType: number, value: string) {
    this.filterType = filterType;
    this.value = value;
  }

  public Criterion(i: number, str: string) {
    this.filterType = i;
    this.value = str;
  }

  public getFilterType(): number {
    return this.filterType;
  }

  public setFilterType(i: number): void {
    this.filterType = i;
  }

  public getValue(): string {
    return this.value;
  }

  public setValue(str: string): void {
    this.value = str;
  }
}
