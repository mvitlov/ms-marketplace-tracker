export interface AppTheme {
  title: string;
  theme: string;
  color: string;
}
