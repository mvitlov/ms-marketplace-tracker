export interface File {
  assetType: string;
  source: string;
}
