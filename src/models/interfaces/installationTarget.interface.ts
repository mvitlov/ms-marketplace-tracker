export interface InstallationTarget {
  target: string;
  targetVersion: string;
}
