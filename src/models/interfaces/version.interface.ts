import { File } from './';

export interface Version {
  assetUri: string;
  fallbackAssetUri: string;
  files: Array<File>;
  flags: string;
  lastUpdated: string;
  version: string;
}
