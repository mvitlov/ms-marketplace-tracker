export interface Publisher {
  displayName: string;
  flags: string;
  publisherId: string;
  publisherName: string;
}
