export interface AppLanguage {
  short: string;
  long: string;
}
