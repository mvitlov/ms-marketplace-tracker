export { File } from './file.interface';
export { Version } from './version.interface';
export { Publisher } from './publisher.interface';
export { Statistic } from './statistic.interface';
export { InstallationTarget } from './installationTarget.interface';
export { AppTheme } from './app-theme.interface';
export { AppLanguage } from './app-language.interface';
