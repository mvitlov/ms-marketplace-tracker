import { Criterion } from './criterion';

export class Filter {
  private criteria: Array<Criterion> = null;
  private pageNumber: number = 1;
  private pageSize: number = 100;
  private sortBy: number = 0;
  private sortOrder: number = 0;
  private direction: number = 2;

  constructor(
    criteria: Array<Criterion>,
    direction?: number,
    pageNumber?: number,
    pageSize?: number,
    sortBy?: number,
    sortOrder?: number
  ) {
    this.criteria = criteria;
    this.direction = direction;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.sortBy = sortBy;
    this.sortOrder = sortOrder;
  }

  public Filter(list: Array<Criterion>) {
    this.criteria = list;
  }

  public getCriteria(): Array<Criterion> {
    return this.criteria;
  }

  public setCriteria(list: Array<Criterion>): void {
    this.criteria = list;
  }

  public getPageNumber(): number {
    return this.pageNumber;
  }

  public setPageNumber(i: number): void {
    this.pageNumber = i;
  }

  public getPageSize(): number {
    return this.pageSize;
  }

  public setPageSize(i: number): void {
    this.pageSize = i;
  }

  public getSortBy(): number {
    return this.sortBy;
  }

  public setSortBy(i: number): void {
    this.sortBy = i;
  }

  public getSortOrder(): number {
    return this.sortOrder;
  }

  public setSortOrder(i: number): void {
    this.sortOrder = i;
  }
}
