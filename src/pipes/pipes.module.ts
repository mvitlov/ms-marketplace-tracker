import { NgModule } from '@angular/core';

import { NumFormatPipe } from './num-format/num-format';
import { IonicPageModule } from 'ionic-angular';
import { SortPipe } from './sort/sort';
import { FilterTargetPipe } from './filter-target/filter-target';
@NgModule({
  declarations: [NumFormatPipe, SortPipe, FilterTargetPipe],
  imports: [IonicPageModule],
  exports: [NumFormatPipe, SortPipe, FilterTargetPipe]
})
export class PipesModule {}
