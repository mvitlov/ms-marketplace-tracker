import { Pipe, PipeTransform } from '@angular/core';
import { Extension } from '../../models/extension.model';

/**
 * Pipe for filtering extension install targets.
 */

@Pipe({
  name: 'filterTarget'
})
export class FilterTargetPipe implements PipeTransform {
  transform(value: Extension[], filter: any) {
    if (value && value.length && filter) {
      return value
        .filter(ext => {
          switch (ext.installTarget) {
            case 'vs': {
              if (filter.filter.vs) return ext;
              break;
            }
            case 'code': {
              if (filter.filter.code) return ext;
              break;
            }
            case 'azure': {
              if (filter.filter.azure) return ext;
              break;
            }
          }
        })
        .sort((ext1, ext2) => {
          switch (filter.sort) {
            case 4:
              return ext2.totalInstalls - ext1.totalInstalls;
            case 1:
              return new Date(ext2.lastUpdate).getTime() - new Date(ext1.lastUpdate).getTime();
            case 3:
              return ext1.publisherName.toLowerCase().localeCompare(ext2.publisherName.toLowerCase());
            case 2:
              return ext1.name.toLowerCase().localeCompare(ext2.name.toLowerCase());
            case 10:
              return new Date(ext2.firstRelease).getTime() - new Date(ext1.firstRelease).getTime();
            case 12:
              return ext2.rating - ext1.rating;
          }
        });
    }
    return value;
  }
}
