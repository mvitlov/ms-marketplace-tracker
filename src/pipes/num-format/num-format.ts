import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the NumFormatPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'numFormat'
})
export class NumFormatPipe implements PipeTransform {
  transform(input: any, args?: any): any {
    var exp,
      suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

    if (Number.isNaN(input)) {
      return null;
    }

    if (input < 1000) {
      return +input.toFixed(args);
    }

    exp = Math.floor(Math.log(input) / Math.log(1000));

    return +(input / Math.pow(1000, exp)).toFixed(args) + suffixes[exp - 1];
  }
}
